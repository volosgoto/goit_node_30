const express = require("express");
const dotenv = require("dotenv");
const logger = require("./middlewares/logger");

let app = express();

// Console colors
// let { colors } = require("./helpers/index");
require("./helpers/index").colors;

// Set config-path
dotenv.config({ path: "./config/config.env" });

// Middlewares
app.use(logger);

// Routes files
const books = require("./routes/books");
const users = require("./routes/users");
app.use("/api/v1/books", books);
// app.use("/api/v1/users", logger, users);
app.use("/api/v1/users", users);

let { PORT, NODE_ENV, JWT_SECRET_TOCKEN } = process.env;

app.listen(PORT, () => {
    console.log(
        `Server running on port ${PORT} on mode ${NODE_ENV} secret key ${JWT_SECRET_TOCKEN}`
            .bold.cyan
    );
});
