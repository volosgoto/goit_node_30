let express = require("express");
let router = express.Router();

const {
    getAllUsers,
    getSingleUser,
    createUser,
    updateUser,
    removeUser,
} = require("../controllers/users");

router.get("/", getAllUsers);
router.get("/:id", getSingleUser);
router.post("/", createUser);
router.put("/:id", updateUser);
router.delete("/:id", removeUser);

module.exports = router;
