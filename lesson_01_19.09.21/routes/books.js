let express = require("express");
let router = express.Router();

// let books = require("../controllers/books");
const {
    getAllBooks,
    getSingleBook,
    createBook,
    updateBook,
    removeBook,
} = require("../controllers/books");

router.get("/", getAllBooks);
router.get("/:id", getSingleBook);
router.post("/", createBook);
router.put("/:id", updateBook);
router.delete("/:id", removeBook);

// router.route("/").get(getAllBooks);
// router.route("/:id").get(getSingleBook);
// router.route("/").post(createBook);
// router.route("/:id").put(updateBook);
// router.route("/:id").delete(removeBook);

module.exports = router;
