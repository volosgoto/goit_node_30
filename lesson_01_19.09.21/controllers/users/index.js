let getAllUsers = require("./getAllUsers");
let getSingleUser = require("./getSingleUser");
let createUser = require("./createUser");
let updateUser = require("./updateUser");
let removeUser = require("./removeUser");

module.exports = {
    getAllUsers,
    getSingleUser,
    createUser,
    updateUser,
    removeUser,
};
