// GET
exports.getAllBooks = (req, res, next) => {
    console.log("All books");
};

// GET
exports.getSingleBook = (req, res, next) => {
    console.log("Single Book");
};

// POST
exports.createBook = (req, res, next) => {
    console.log("Book is created");
};

// Update
exports.updateBook = (req, res, next) => {
    console.log("Book is updated");
};

// Remove
exports.removeBook = (req, res, next) => {
    console.log("Book is removed");
};
