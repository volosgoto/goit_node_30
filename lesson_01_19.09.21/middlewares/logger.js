let fs = require("fs/promises");
let path = require("path");

console.log("path.join() : ", path.join());
// outputs .
console.log("path.resolve() : ", path.resolve());

let logger = async (req, res, next) => {
    let { originalUrl, method, protocol } = req;

    let date = new Date().toLocaleString();
    console.log(date);
    let logs = `${date}: ${method} ${protocol}://${req.get(
        "host"
    )}${originalUrl}\n`;
    console.log(logs);

    let logsPath = path.join(__dirname, "..", "logs", "serverLogs.txt");
    await fs.appendFile(logsPath, logs, "utf-8");

    next();
};

module.exports = logger;
